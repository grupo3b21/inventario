const express = require ('express');
const router = express.Router();

//jmportar módulo 	para MongoDb (Mongoose)
// const mongoose = require ('mongoose');

//import la ruta de la conexión a la base de datos
const conectar = require('./config/db');

// const libroSchema = mongoose.Schema({
// 	id: Number,
// 	titulo: String,
// 	genero: String,
// 	autor: String,
// 	cantidad: Number,
// 	img: String,
// 	valor: Number
// });


// const modelLibro = mongoose.model('libros', libroSchema)

//crear
// modelLibro.create(
// 		{
// 			id:156,
// 			titulo: "mil años de humedad",
// 			genero: "suspenso",
// 			autor: "Mirando",
// 			cantidad: 15,
// 			img: "http://asdasd.com"
// 			valor: 23500
// 		},
// 		(error) => {
// 			if (error) return console.log(error);
// 		}
// )

//Buscar
// modelLibro.find((error,libros) =>{
// 	if(error) return console.log(error);
// 	console.log(libros);

// });

//Actualizar
 // modelLibro.updateOne({id:55}, {titulo:"General abnegado por mala conducta ",cantidad:999}, error =>{
 // 	if (error) return console.log('Errrooorrr de update');
 // });

//Eliminar
// modelLibro.deleteOne({id:56},(error) =>{
// 	if (error) return console.log('Errrooorrr');
// })


//conexión usando variable de entorno

// mongoose.connect(process.env.url_mongo2)
// 	.then(function(){console.log("Conexión bien realizada con MongoDb Atlas")})
// 	.catch(function(e){console.log(e)})

let app = express();

//USO DE ARCHIVOS JSON EN LA APP
app.use(express.json());

//CORS Reglas de seguridad para control HTTP
const cors = require('cors');

app.use(cors());

//SOLICITUDES AL CONTROLADOR
const crudLibros = require('./controladores/controlLibros');

//ESTABLECER RUTAS CON RESPECTO AL CRUD

//Crear
router.post('/apirest/', crudLibros.crear);

//Buscar
router.get('/apirest/', crudLibros.obtener);

//Actualizar
router.put('/apirest/:id', crudLibros.actualizar);

//Eliminar
router.delete('/apirest/:id', crudLibros.eliminar);

// app.use('/', function(req,res){
// 	res.send('Hola')
// });

app.use(router);
// router.get('/metget', function(req,res){
// 	res.send("Hola método get")
// });

// router.post('/metpost', function(req,res){
// 	res.send("Hola método post")

// 	// //conexión
// 	// console.log("Mensaje antes de la conexión a la DB ");
// 	// const user = 'Soporte_Inventario';
// 	// const password = 'Darkdemon123';
// 	// const db = 'Inventario_Libreria';
// 	// const url = `mongodb+srv://${user}:${password}@inventariolibreria.hzci9.mongodb.net/${db}?retryWrites=true&w=majority`;

// 	// //establecer conexión

// 	// mongoose.connect(url)
// 	// 	.then(function(){console.log("Conexión bien realizada con MongoDb Atlas")})
// 	// 	.catch(function(e){console.log(e)})

// });


//conexión con la base de datos

// const user = 'Soporte_Inventario';
// const password = 'Darkdemon123';
// const db = 'Inventario_Libreria';
// const url = `mongodb+srv://${user}:${password}@inventariolibreria.hzci9.mongodb.net/${db}?retryWrites=true&w=majority`;

// //establecer conexión

// mongoose.connect(url)
// 	.then(function(){console.log("Conexión bien realizada con MongoDb Atlas")})
// 	.catch(function(e){console.log(e)})

conectar();
// app.listen(4000);

//carpeta del front
app.use(express.static('public'));
app.listen(process.env.PORT);

console.log("Escucho desde puerto 4000") 