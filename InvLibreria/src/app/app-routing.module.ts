import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
     
//import { CommonModule } from '@angular/common';

const rutas: Routes = [

	// {
	// 	path: 'home',
	// 	loadChildren: () => import('./@public/pages/home/home.module').then(m => m.HomeModule )	
	// },

	// {
	// 	path: 'contact',
	// 	loadChildren: () => import('./@public/pages/contact/contact.module').then(m => m.ContactModule )	

	// },

	{
		path: '',
		redirectTo: 'home',
		pathMatch: 'full'
	},

	{
		path: '**',
		redirectTo: 'home',
		pathMatch: 'full'
	}

];

@NgModule({
  declarations: [],
  imports: [
    //CommonModule
    RouterModule.forRoot(rutas,
    {
    	useHash : false,
    	scrollPositionRestoration: 'enabled'
    }) 
    
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
