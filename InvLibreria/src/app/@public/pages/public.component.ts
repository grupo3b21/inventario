import { Component, OnInit } from '@angular/core';
import { LibrosService } from 'src/app/@services/libros.service';

@Component({
  selector: 'app-public',
  templateUrl: './public.component.html',
  styleUrls: ['./public.component.css']
})
export class PublicComponent implements OnInit {

	//Atributos
	titulo : any;
	genero : any;
	autor : any;
	cantidad : any;
	valor : any;
	imagen : any;

	decision : boolean = false;
	libros : any;
	// libros : any[] = [

	// 	{
	// 		id:1,
	// 		titulo: "199 sombras de la gaysha",
	// 		genero: "rockstar",
	// 		autor: "Jimmy Hendrix",
	// 		cantidad: 23,
	// 		valor: 38900,
	// 		imagen : "http://localhost:4200/assets/Logo.png"
	// 	},
	// 	{
	// 		id:2,
	// 		titulo: "500 luces de la San Juan",
	// 		genero: "Sofimática",
	// 		autor: "Jimmy Neutron",
	// 		cantidad: 3,
	// 		valor: 78900,
	// 		imagen: "http://localhost:4200/assets/Logo.png"
	// 	}		
	// ]

  constructor(private _libro : LibrosService) { }

  ngOnInit(): void {
	  this.obtenerLibro();
  }

  //Funciones
  guardarLibro(){
  	console.log(this.titulo);
  	console.log(this.genero);
  	console.log(this.autor);
  	console.log(this.valor);

  	if(this.titulo != undefined){
  		let indice = this.libros.length + 1;
  		let variableAuxiliar = {
			id: indice,
			titulo:this.titulo,
			genero:this.genero,
			autor:this.autor,
			valor:this.valor,
			cantidad : this.cantidad,
			imagen:this.imagen
  	}

  	//this.libros.push(variableAuxiliar)
	  this._libro.guardarDatos(variableAuxiliar)
	  	.subscribe( datos => {
			  console.log("Respuesta : " , indice, datos);
			  this.obtenerLibro();
		  })
  	}

  }
  deleteLibro(indice:any){
	  console.log("Dato eliminado :", indice )
	//   this.libros.splice(indice,1);
	this._libro.eliminarDatos(indice)
		.subscribe (datos => {
			console.log("Dato: " , indice , " Eliminado" , datos);
			this.obtenerLibro();
		})	
  }

  obtenerLibro(){
	  this._libro.obtenerDatos()
	  .subscribe(datos => {
		  this.libros = datos;
		  console.log(this.libros);
	  })
  }

}
