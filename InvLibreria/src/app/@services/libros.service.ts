import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LibrosService {
    //Atributos
    url : any = 'http://localhost:4000/apirest/'
  constructor(private http : HttpClient ) {  }

    //Funciones

    //leer
    obtenerDatos(){
      return this.http.get(this.url);
    }
    //eliminar
    eliminarDatos(id:any){

      return this.http.delete(this.url + id);
    }

    //crear
    guardarDatos(libro: any){
      return this.http.post(this.url,libro)
    }


}
