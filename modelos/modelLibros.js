//jmportar módulo 	para MongoDb (Mongoose)
const mongoose = require('mongoose');

const libroSchema = mongoose.Schema({
	id: Number,
	titulo: String,
	genero: String,
	autor: String,
	cantidad: Number,
	imagen: String,
	valor: Number
});

// Para utilizar en archivos externos debo exportar como módulo
module.exports = mongoose.model('libros', libroSchema);