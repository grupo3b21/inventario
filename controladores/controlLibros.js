//// importar el modelo (squema) de libros  que definimos
const modelLibro = require('../modelos/modelLibros');

//Exportar en diferentes variables del método CRUD
//Create
exports.crear = async (req, res) => {

    try {
        
        let libro;

        libro = new modelLibro({

        	id: 45,
        	titulo: "El recuerdo que seremos",
        	genero: "Romance",
        	autor: "Franklin",
        	cantidad: 12,
        	img: "http://pinterest.com",
        	valor: 25000

        });

        await libro.save();

        res.send(libro);

    } catch (error) {
        console.log(error);
        res.status(500).send('Error al guardar el libro');
    }
}

//Buscar
exports.obtener = async(req,res) =>{

	try{
		const libro = await modelLibro.find();
		res.json(libro);
	}catch(error){
		console.log(error);
		res.status(500).send('Error al obtener la data');

	}
}

//ACTUALIZAR

exports.actualizar = async(req,res) => {
	try{
		const libro = await modelLibro.findById(req.params.id);

		if (!libro){
			console.log(producto);
			res.status(404).json({msg : 'El libro no existe'});
		
		} 
		else{
			await modelLibro.findByIdAndUpdate({_id: req.params.id}, {cantidad : 90});
			res.json({msg: 'Libro actualizado satisfactoriamente'});
		}
	}catch(error){
		console.log(error);
		res.status(500).send('Error al actualizar el libro');
	}
}

//ELIMINAR
exports.eliminar = async(req,res)=>{
	try{
		const libro = await modelLibro.findById(req.params.id);

		if(!libro){
			console.log(libro);
			res.status(404).json({msg: 'Libro no existe'});			
		}
		else{
				await modelLibro.findByIdAndRemove(req.params.id);
				res.json({mensaje: 'Libro eliminado'})	;
			}
	}catch(error){
		console.log(error);
		res.status(500).send('Error al eliminar el libro');
	}
}