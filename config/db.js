//jmportar módulo 	para MongoDb (Mongoose)
const mongoose = require ('mongoose');

//importar variables
require('dotenv').config({path: 'var.env'});

//función asincrónica
const conectar = async () =>{

	try{
		await mongoose.connect(process.env.url_mongo ,{});
		console.log("Conexión establecida con mongo Atlas desde .config/db.js");
	}catch(error){
	console.log("error en la conexión");
	console.log(error);
	process.exit(1);
	}
}
// expotar para usarlo en otros files
module.exports = conectar;


